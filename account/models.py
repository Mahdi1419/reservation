from django.db import models
import uuid
from django.core.validators import MaxValueValidator, MinValueValidator

# utils
from api_app.utils import SuperModel
# Create your models here.

class Account(models.Model, SuperModel):
    """
    Customers and patients model\n
    Properties: 
    * id
    * phone
    * id card number
    * name 
    * family 
    * age
    """
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)

    # Login, registration and authentication are done by phone
    phone = models.CharField(max_length=11, unique=True)

    age = models.IntegerField(
        validators=[MaxValueValidator(150), MinValueValidator(1)]
    )


    id_number = models.CharField(max_length=10, blank=False, null=False, unique=True)
    name = models.CharField(max_length=50, blank=False, null=False)
    family = models.CharField(max_length=100, blank=False, null=False)

    two_step_veriflicaton = models.BooleanField(default=False)
    password = models.CharField(max_length=255 ,null=True, blank=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    # class Meta:
    #     constraints = [
    #         models.CheckConstraint(check=models.Q(age__gte=18), name='age_gte_18'),
    #     ]


    def __str__(self):

        return f'{self.name} {self.family} > {self.phone}'


class Token(models.Model, SuperModel):
    """
    Accounts Token model\n
    Properties: 
    * account => instance of 'Account' model
    * token

    """
    account = models.OneToOneField(Account, on_delete=models.CASCADE, related_name='token')
    token = models.CharField(max_length=255, unique=True, null=False, blank=False)

    def __str__(self) -> str:
        return self.account.phone


class VerifyCode(models.Model, SuperModel):
    
    """
    Model for temporary storage of validation and authentication codes\n
    Properties:
    * phone
    * code_for => Verify Code must be one of this (login, register or delete)
    * code => Five digit code
    * expire_time => Code expiration time
    * expire_action_time => Code expiration time to perform the relevant operation
    """
    phone = models.CharField(max_length=11)
    code_for = (
        ('login','Login'),
        ('register', 'Register'),
        ('delete', 'Delete'),
        ('set-password', 'Set password'),
    )
    code_for = models.CharField(max_length=15, choices=code_for)
    code = models.IntegerField(
        validators=[MaxValueValidator(99999), MinValueValidator(10000)]
    )

    expire_time = models.IntegerField()
    re_create = models.BooleanField(default=True)
    

    class Meta:
        # two instance can not be same with this fields
        unique_together = ('phone', 'code_for',)

    def __str__(self):
        return f'{self.phone} > {self.code}'