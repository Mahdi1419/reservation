
from jose import jwe
import json

TOKEN_HASH_256_KEY = 'Xp2s5v8y/B?D(G+KbPeShVmYq3t6w9z$'

encoded = jwe.encrypt(json.dumps({"uid" : 'user.uid.hex'}), TOKEN_HASH_256_KEY, algorithm='dir', encryption='A256GCM')

print(encoded)