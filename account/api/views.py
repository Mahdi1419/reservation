import random

from django.views import View
from Reservation import settings
# models
from ..models import Account, VerifyCode

# Serilizers
from account.api.serializers import LoginSerializer

# data transforms
from account.api.transform_data import LoginTransform

# utils
from api_app.response import StaticResponse, ApiResponse
from api_app.utils import (
    addToTimeUnixTimeStamp,
    check_code, check_phone,
    checkListEquality,
    getUnixTime
)

# Create your views here.


class PanelView(View):
    pass


class VerifyCodeView(View):
    """
    Create and validate verify codes
    """
    
    def post(self, request, mode):
        """
        Create and send verify code\n
        method: post\n
        parameter: [phone,]\n
        """
        try:
            # needed prameter
            required_params = ['phone']

            # get request.POST parameter
            params = request.POST

            # Comparison request.POST parameter with needed parameter
            is_match = checkListEquality(required_params, params)

            # if not match
            if not is_match:
                return StaticResponse.InvaliRequestParameter

            # check verify code is exist or not
            verify_obj = VerifyCode.is_exist({
                "phone": params.get('phone'),
                "code_for": mode,
            })

            if verify_obj:

                if not verify_obj.re_create:
                    return StaticResponse.VerifyReCreate
                # check code expire time
                if getUnixTime() > verify_obj.expire_time:
                    # if expired delete object
                    verify_obj.delete()
                else:
                    # if not expired return this:
                    return StaticResponse.VerifyCodeAlredyExist

            # get unix time stamp
            unix = getUnixTime()

            # create a verify object

            # set verify code expire time
            match mode:
                case 'login':
                    expire_time = addToTimeUnixTimeStamp(unix, min=20)

                # case 'register':
                #     expire_time = addToTimeUnixTimeStamp(unix, min=5)

                case 'delete':
                    expire_time = addToTimeUnixTimeStamp(unix, min=2)

                case _:
                    raise Exception('mode undefined')

            # creating verify code
            VerifyCode.objects.create(
                phone=params.get('phone'),
                code_for=mode,
                code=random.randint(10000, 99999),
                expire_time=expire_time,
            )

            return StaticResponse.VerifyCodeSent

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError


class AuthView(View):

    """
    Login and Registration View
    """

    def post(self, request):
        """
        account login handle\n
        method: post\n
        parameter: [phone, code => verify code]\n
        return: account auth token
        """
        try:
            # needed prameter
            required_params = ['phone', 'code']

            # get request.POST parameter
            params = request.POST

            # Comparison request.POST parameter with needed parameter
            is_match = checkListEquality(required_params, params)

            # if not match
            if not is_match:
                return StaticResponse.InvaliRequestParameter

            # check phone number format
            phone_validation = check_phone(params.get('phone'))
            if not phone_validation:
                return StaticResponse.PhoneValidationError

            # check verify code format
            verify_code_validation = check_code(params.get('code'))
            if not verify_code_validation:
                return StaticResponse.VerifyCodeValidationError

            # check verify code
            verify_obj:VerifyCode = VerifyCode.is_exist({
                "phone": params.get('phone'),
                "code": int(params.get('code')),
                "code_for": 'login',
            })

            # if not exist any verify code for login
            if not verify_obj:
                return StaticResponse.PhoneNotVerify
            
            # if verify code expired
            if getUnixTime() > verify_obj.expire_time:
                verify_obj.delete()
                return StaticResponse.VerifyCodeIsExpire
            
            # get account with phone number
            account_obj = Account.is_exist({
                "phone": params.get('phone'),
            })


            if not account_obj:
                # if account was not exist, 
                # we change verify code mode to 'register' and adding to expire time, 
                verify_obj.code_for = 'register'
                verify_obj.expire_time = addToTimeUnixTimeStamp(verify_obj.expire_time, min=3)
                verify_obj.save()
                
                # in this case returned status code is 404
                return StaticResponse.AccountNotFound

            # if was not problem in process
            # get login date from serializer
            serialized_data = LoginSerializer(instance=account_obj)

            print(serialized_data)
            # transform data
            transform_data = LoginTransform(serialized_data)

            # delete verify object
            verify_obj.delete()

            
            # return login data
            return ApiResponse({
                "data": transform_data,
                "message": "Login completed successfully"
            }, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    def put(self, request):
        """
        account registration handle\n
        method: post\n
        parameter: [phone, code]\n
        """
        try:
            # needed prameter
            required_params = ['phone', 'name',
                               'family', 'age', 'id_number', 'code']

            # get request.PUT parameter
            params = request.PUT

            # Comparison request.PUT parameter with needed parameter
            is_match = checkListEquality(required_params, params)

            # if not match
            if not is_match:
                return StaticResponse.InvaliRequestParameter

            # check phone number format
            phone_validation = check_phone(params.get('phone'))
            if not phone_validation:
                return StaticResponse.PhoneValidationError

            # check verify code format
            verify_code_validation = check_code(params.get('code'))
            if not verify_code_validation:
                return StaticResponse.VerifyCodeValidationError

            # get account with phone number
            account_obj = Account.is_exist({
                "phone": params.get('phone'),
            })

            if account_obj:
                return StaticResponse.AccountAlreadyExist

            # check verify code
            verify_obj = VerifyCode.is_exist({
                "phone": params.get('phone'),
                "code": int(params.get('code')),
                "code_for": 'register',

            })

            # if not exist any verify code for register
            if not verify_obj:
                return StaticResponse.PhoneNotVerify

            # if verify code expired
            if getUnixTime() > verify_obj.expire_time:
                verify_obj.delete()
                return StaticResponse.VerifyCodeIsExpire

            # if was not problem in process
            # create new Account

            new_account = Account(
                phone=params.get('phone'),
                name=params.get('name'),
                family=params.get('family'),
                age=params.get('age'),
                id_number=params.get('id_number'),
            )

            new_account.save()

            # after create new account
            # set verify object re_create to false and save (for register )
            verify_obj.re_create = False
            verify_obj.save()

            # get login date from serializer
            serialized_data = LoginSerializer(instance=new_account)

            # transform data
            transform_data = LoginTransform(serialized_data)

            # return login data
            return ApiResponse({
                "data": transform_data,
                "message": "Registration was successfully"
            }, status=201)


        
        except Exception as e:
            if settings.DEBUG:
                print(e)
            # return server error if there was an unknown error  
            return StaticResponse.ServerError
