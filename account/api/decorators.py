
from api_app.response import ApiResponse

def authRequired(func):
    def wrapper(view, request, *args, **kwargs):
        if not request.auth:
            return ApiResponse({"message": "Auth required (or token invalid)"}, status=403)
        response = func(view, request, *args, **kwargs)
        return response
    return wrapper


def adminRequired(func):
    def wrapper(view, request, *args, **kwargs):
        if not request.auth or not request.auth_user.is_admin:
            return ApiResponse({"message": "Only admins acssec to this url"}, status=403)
        response = func(view, request, *args, **kwargs)
        return response
    return wrapper

