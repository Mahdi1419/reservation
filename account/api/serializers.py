
from api_app.Serializer import RelationTypes, Serializer
from ..models import Account

class LoginSerializer(Serializer):
    token = Serializer.RelatedField(relation_type=RelationTypes.OneToOne,fields=['token'], field_name='get_token')

    class Meta:
        model = Account
        fields = ['get_token']
