
from api_app.response import ApiResponse

class AuthRequired:
    def dispatch(self, request, *args, **kwargs):
        if request.auth_user == None:
            return ApiResponse({"message": "Auth required (or token invalid)"}, status=403)

        return super().dispatch(request, *args, **kwargs)