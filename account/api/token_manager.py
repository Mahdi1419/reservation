import json
from jose import jwe
from account.models import Account
from Reservation import settings


class TokenManager:

    staticmethod
    def create_user_token(account: Account):
        try:    
            encoded = jwe.encrypt(json.dumps({"id" : account.id.hex}), settings.TOKEN_HASH_256_KEY, algorithm='dir', encryption='A256GCM')
            return encoded.decode('UTF-8')
        except Exception as e:
            print(e)
            raise Exception("can't create token")


    staticmethod
    def decode_user_token(token: str):
        try:
            decoded = jwe.decrypt(token, settings.TOKEN_HASH_256_KEY)
            data = json.loads(decoded)
            return data
        except:
            raise("can't decode token")


