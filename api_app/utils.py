
# imports
import json
import random
import string
import time
import datetime
from uuid import UUID
from django.core.exceptions import MultipleObjectsReturned
from django.core.checks import messages

from Reservation import settings


class SuperModel:
    """
    SuperModel class Adds more methods to models
    """
    @classmethod
    def is_exist(cls, filter: dict):
        """
        Check the existence of the object using filter values and return that\n
        args: 
        * filter => must be 'dict'

        """

        if len(filter) < 1:
            return False

        obj = cls.objects.filter(**filter)

        # if find any instance
        if obj.exists():

            if len(obj) <= 1:
                # if there was one instance
                obj = obj.get(**filter)
            return obj
        return False
    
    
    def custom_update(self, data: dict, except_update=[]):
        
        try:
            for key, value in data.items():
                if key not in except_update:
                    if hasattr(self, key):
                        setattr(self, key, value)
            self.save()
            
            return self
            
        except Exception as e:
            if settings.DEBUG:
                print(e)
            return False

    # @classmethod
    # def custom_update(self, filter: dict, data: dict, except_update=[]):
        
    #     """
    #     Updates the specified model instance using the keyword arguments as the model property attributes and values.\n
    #     Example usage:
    #         * update(filter, updated_data)

    #     return: Updated obj
    #     raise : return False
    #     """
    #     try:
    #         obj = cls.objects.get(**filter)
    #         if obj:
    #             for attr, val in data.items():
    #                 if attr not in except_update:
    #                     if hasattr(obj, attr):
    #                         setattr(obj, attr, val)
    #             obj.save()

    #             return obj
            
    #         return False
    #     except cls.DoesNotExist:
    #         return None

    #     except Exception as e:
    #         print(e)
    #         return False



def getUnixTime():
    """
    get unix time stamp and return it!
    """

    unix = time.time()

    # deleting decimals
    unix = int(unix)

    return unix


def addToTimeUnixTimeStamp(unixTimeStamp: int, sec: int = False, min: int = False, hour: int = False, day: int = False):
    """
    Add (seconds, minutes, hours, days) to Unix time\n
    args:
    * unixTimeStamp => int
    * seconds => int
    * minutes => int
    * hours => int
    * days => int
    """
    if sec and int(sec) != 0:
        unixTimeStamp += int(sec)
    if min and int(min) != 0:
        unixTimeStamp += int(min*60)
    if hour and int(hour) != 0:
        unixTimeStamp += int(hour*3600)
    if day and int(day) != 0:
        unixTimeStamp += int(day*86400)
    return unixTimeStamp


def checkListEquality(list1, list2):
    """
    Check the equality of the two lists\n
    args:
    * list1
    * list2

    return: Boolean
    """

    valid = True

    if len(list1) != len(list2):
        return False

    for param in list2:
        if param in list1:
            pass
        else:
            valid = False
    return valid


def check_phone(phone):
    """
    Phone number validation\n
    prameter:
    * phone
    """

    try:
        int(phone)
        if len(phone) != 11:
            return False
        return True
    except ValueError:
        return False


def check_code(code):
    """
    code validation\n
    prameter:
    * code
    """

    try:
        int(code)
        if len(code) != 5:
            return False
        return True
    except ValueError:
        return False


def checkUUidV4(uuid: str):
    """
    validate uuid.v4 
    """
    try:
        val = UUID(uuid, version=4)
        return True
    except ValueError:
        # If it's a value error, then the string
        # is not a valid hex code for a UUID.
        return False


def json_loads(data, mode):

    try:
        loads = json.loads(data)
        if isinstance(loads, mode):
            return loads
        return False
    except json.decoder.JSONDecodeError:
        return False


def get_random_string(length, start_with=''):
    """
    creating random strings with desired length
    """

    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))

    return f'{start_with}{result_str}'


def create_receipt_token(length: int = 8, start_with: str = 'receipt-'):
    """create receipt token
    
    Args:
        length (int, optional): Defaults to 8.
        start_with (str, optional): Defaults to 'receipt-'.

    Returns:
        str : receipt token 
    """

    range_start = 10**(length-1)
    range_end = (10**length)-1
    random_code = random.randint(range_start, range_end)
    return f'{start_with}{random_code}'


def DateValidate(date_text):
    """Validate Date format

    Args:
        date_text (str): example => '2021-01-01'

    Returns:
        bool : True/False

    """
    try:
        date_obj  = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        return date_obj 
    except ValueError:
        return False
    

def TimeValidate(time_text):
    """Validate Time format

    Args:
        date_text (str): example => '00:00:00'

    Returns:
        bool : True/False

    """
    try:
        time_obj = datetime.datetime.strptime(time_text, '%H:%M:%S').time()
        return time_obj
    except ValueError:
        return False


def cleanDicts(params:list, valuse:any):
    """remove all data from dict except 'params'

    Args:
        params (dict | QueryDict)
        dict (dict)

    Returns:
        dict
    """    
    
    new_params = dict()
    
    for param in params:
        if param in valuse.keys():
            new_params.update({param:valuse[param]})  
    
    
    return new_params
    
    

class MessageHandler:
    """Handle Messages"""
    messages = list()

    def add_message(self, message: str):
        """add message

        Args:
            message (str)
        """
        self.messages.append(message)

    def get_all_messages(self, name: str = 'messages'):
        """get all message

        Args:
            name (str, optional): Defaults to 'messages'.

        Returns:
            dict : dict of all messages
        """
        
        if len(self.messages) < 1:
            return False

        return {
            name: self.messages
        }


