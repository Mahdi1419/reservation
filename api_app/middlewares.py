from io import BytesIO, StringIO
from django.http import QueryDict
from django.http.multipartparser import MultiPartParser
import json
from account.api.account_manager import AccountManager
from account.models import Account
# from account.api.account_controller import AccountController

from api_app.auth_config import auth_check_views


class HttpSetUnsupportedMethods:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):

        # if request method not equal with 'post' and 'get
        if not request.method in ['POST', 'GET']:

            # if request content type == multipart/formdata
            if request.META.get('CONTENT_TYPE', '').startswith('multipart'):

                # extract request data and files
                requestMethod, requestFiles = MultiPartParser(request.META, BytesIO(request.body),
                                                              request.upload_handlers, request.encoding).parse()

                # set data to request method
                setattr(request, request.method, requestMethod)

                # set files to request
                setattr(request, '_files', requestFiles)

            else:
                requestMethod = QueryDict(
                    request.body, encoding=request.encoding)
                setattr(request, request.method, requestMethod)


class AuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def get_token(self, request):
        """
        extract app-token from request headers or request methods
        """

        token = request.headers.get('app-token') or request.GET.get('app-token') or None
        return token

    def process_view(self, request, view_func, view_args, view_kwargs):

        # get token
        token = self.get_token(request)
        if token != None:

            # get user by token
            user = AccountManager.getAccountByToken(token)
            if user != None:
                setattr(request, 'auth', True)
                setattr(request, 'auth_user', user)

            else:
                setattr(request, 'auth', False)
                setattr(request, 'auth_user', None)
        else:
            setattr(request, 'auth', False)
            setattr(request, 'auth_user', None)
