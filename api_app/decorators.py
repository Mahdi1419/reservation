
import functools

from django.http.request import QueryDict
from api_app.utils import checkListEquality
from .response import ApiResponse, StaticResponse

def parmsCheck(needed_params:list, method:str):
    """check request parameter

    Args:
        needed_params (list)
        method (str): POST, GET, PUT, DELETE
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapper(view, request, *args, **kwargs):
            
            # get request.POST parameter
            params = getattr(request, method)

            # Comparison request.POST parameter with needed parameter
            is_match = checkListEquality(needed_params, params)

            # if not match
            if not is_match:
                return StaticResponse.InvaliRequestParameter
                
            response = func(view, request, *args, **kwargs)
            
            return response
        return wrapper
    return decorator
    


# def cleanRequest(valid_params:list, method:str):
#     """check request parameter

#     Args:
#         params (list)
#         method (str): POST, GET, PUT, DELETE
#     """
#     def decorator(func):
#         @functools.wraps(func)
#         def wrapper(view, request, *args, **kwargs):
            
#             # get request.POST parameter
#             params = dict(getattr(request, method).copy())
            
            
            
#             for param in params:
#                 if not param in valid_params:
#                     print(params.pop(param))


            
#             response = func(view, request, *args, **kwargs)
            
#             return response
#         return wrapper
#     return decorator
    