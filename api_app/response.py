
from enum import Enum
from django.http import JsonResponse

from reserve.models import Section


def ApiResponse(data, status=200):
    """Api response 

    Args:
        data (any)
        status (int, optional): Status code. Defaults to 200.

    Returns:
        JsonResponse
    """
    return JsonResponse(data, status=status)


class StaticResponse:
    """
    static json responses
    """

    ServerError = ApiResponse({
        "message": 'sertver error'
    }, status=500)

    PhoneValidationError = ApiResponse({
        "message": 'phone number not valid'
    }, status=211)

    VerifyCodeValidationError = ApiResponse({
        "message": 'verify code not valid'
    }, status=211)

    InvaliRequestParameter = ApiResponse({
        "message": 'invalid parameter'
    }, status=403)

    AccountNotFound = ApiResponse({
        "message": 'Account dose not exist'
    }, status=404)

    AccountAlreadyExist = ApiResponse({
        "message": 'Account already exist'
    }, status=409)

    PhoneNotVerify = ApiResponse({
        "message": 'Please verify your phone'
    }, status=403)

    VerifyCodeIsExpire = ApiResponse({
        "message": 'Verify code has been expired'
    }, status=410)

    VerifyCodeAlredyExist = ApiResponse({
        "message": 'verify code already sent'
    }, status=403)

    VerifyCodeSent = ApiResponse({
        "message": "verify code sent"
    }, status=200)

    VerifyReCreate = ApiResponse({
        "message": "you can't request verify code for registration"
    }, status=403)

    AlreadyExist = ApiResponse({
        "message": 'object already exist'
    }, status=409)
    
    SectionNotFount = ApiResponse({
        "message": 'section dose not exist' 
    }, status=404)