from enum import Enum
from django.db.models.query import QuerySet
class WarningHandler:
    """
    Showing warnigs in console/terminal
    """
    
    # set color and reset color code
    WARNING = '\033[93m'  # YELLOW
    RESET = '\033[0m'  # RESET COLOR


    def __init__(self):
        # A list for store messages
        self.warning_list = list()
        
    def add_warning(self, message:str):
        """Adding warnings to warning_list

        Args:
            message (str)
        """
        
        # Checking for duplicates
        if not message in self.warning_list:
            self.warning_list.append(message)


    def show_warnings(self):
        """showing warnings"""
        # printing all messages
        for warning in self.warning_list:
            print(self.WARNING + warning + self.RESET)
    
# instance of WarningHandler for global use 
warning = WarningHandler()

class RelationTypes(Enum):
    """
    Relationship types
    """
    OneToOne = 'OneToOne',
    ManyToOne = 'ManyToOne',
    ManyToMany = 'ManyToMany'
    
class BaseSerializer(type):
    """Metaclass for all Serializers"""
    
    
    def __new__(mcs, name, bases, attrs):
        
        """initialization subclasses of Serializers"""
        cls = type.__new__(mcs, name, bases, attrs)
        cls.Meta = attrs['Meta'] if 'Meta' in attrs else None # set Meta class
        if cls.Meta != None:
            custom_fields = list() # custom fields
            related_fields = list() # related fields
            for key, value in attrs.items():
                match type(value):
                    case cls.RelatedField:
                        
                        # set related fields
                        related_fields.append({
                            "name" : key,
                            "relation_type" : value.relation_type,
                            "fields" : value.fields,
                            "except_fields" : value.except_fields,
                            "field_name" : value.field_name
                        })
                        
                        
                    case cls.CustomField:
                        
                        # set custom fields
                        custom_fields.append({
                            "name" : key,
                            "func_name": value.func_name,
                            "field_name": value.field_name
                        })
                        
                    case _:
                        # anything else, do nothing
                        pass
            
            # set custom fields and related fields to subclass (Serializer)
            cls.custom_fields = custom_fields
            cls.related_fields = related_fields
            
        return cls

class Serializer(metaclass=BaseSerializer):
    def __init__(self, instance=None, many=False):
        # get instance
        self.instance = instance,
        
        # many => Multiple data mode controller
        self.many = many
        
    def __new__(cls, *args, **kwargs):
        
        if cls.__get_attrs(cls, 'Meta') == None:
            raise Exception('Serializer must have a meta class ')
        
        model = cls.Meta.model or None
        instances = kwargs.get('instance') or None
        many = kwargs.get('many') or None
        fields = cls.__get_attrs(cls.Meta, 'fields')
        except_fields = cls.__get_attrs(cls.Meta, 'except_fields')
        custom_fields = cls.custom_fields
        related_fields = cls.related_fields


        # if instance is one object append to list
        if isinstance(instances, model):
            instances = [instances]
        
        # if instance is a QuerySet list, cast to normal list
        if isinstance(instances, QuerySet):
            instances = list(instances)       
        
        # if there is no model, or instance not a normal list
        if model == None or not isinstance(instances, list):
            # return empty list
            return {"data": []}
        
        # create all data 
        all_data = list()
        

        # do this with any inctance in all instancese
        for instance in instances:

            # if instances types not equal with model 
            if not isinstance(instance, model):
                break
            
            all_r_data = {}
            # Set Relateds
            for related_field in related_fields:
                relation_type = related_field.get('relation_type') # related type
                relation_neme = related_field.get('name') # related name
                relation_fields = related_field.get('fields') # related fields
                relation_except_fields = related_field.get('except_fields') # related exept fields
                relation_field_name = related_field.get('field_name') or relation_neme # relation name
                match relation_type:
                    # if relation is one to one relateion
                    case RelationTypes.OneToOne:
                        
                        # if instance have 'relateion_name' field
                        if hasattr(instance, relation_neme):
                            
                            # in a one-to-one relationship, field name must not match the variable name
                            if relation_field_name != relation_neme:
                                
                                # get relation object data
                                r_data = cls.__get_data(getattr(instance, relation_neme), fields=relation_fields, excepted_fields=relation_except_fields)                            
                                
                                # add data to all related data (all_r_data)
                                all_r_data.update({relation_field_name: r_data})
                            else:
                                
                                # if field_name == relation_neme -> relation_neme
                                warning.add_warning('In a one-to-one relationship, you must add a field_name that does not match your variable name')
                        else:
                            # if instance has not any relation with related_name
                            warning.add_warning('instance does not have a field with name \'{0}\''.format(relation_neme))
                            
                    case RelationTypes.ManyToOne:
                        # if instance have 'relateion_name' + '_set' (to access data in ManyToOne relations, we must be add '_set' to end of 'relation_name') field
                        if hasattr(instance, relation_neme + '_set'):
                            
                            # get all of relation data
                            r_list = getattr(instance, relation_neme + '_set').all()
                            
                            # get relation object data
                            r_data = cls.__get_multi_data(r_list, fields=relation_fields, excepted_fields=relation_except_fields)
                            
                            # add data to all related data (all_r_data)
                            all_r_data.update({relation_field_name: r_data})
                        else:
                            # if instance has not any relation with related_name
                            warning.add_warning('instance does not have a field with name \'{0}\''.format(relation_neme))
                    
                    case RelationTypes.ManyToMany:
                        # if instance have 'relateion_name' field
                        if hasattr(instance, relation_neme):
                            
                            # get all of relation data
                            try:
                                r_list = getattr(instance, relation_neme).all()
                                
                                # get relation object data
                                r_data = cls.__get_multi_data(r_list, fields=relation_fields, excepted_fields=relation_except_fields)
                                
                                # add data to all related data (all_r_data)
                                all_r_data.update({relation_field_name: r_data})
                            except Exception as e:
                                warning.add_warning('Instance does not have a field with name \'{0}\' (Maby using wrong relation)'.format(relation_neme))

                        else:
                            # if instance has not any relation with related_name
                            warning.add_warning('Instance does not have a field with name \'{0}\''.format(relation_neme))
                        
                        
            # set custom fields
            custom_field_data = {}
            for custom_field in custom_fields:
                # get name of variable
                name = custom_field.get('name')
                
                # get fuction name
                func_name = custom_field.get('func_name')
                
                field_name = custom_field.get('field_name') or name
                
                # if cls have any fuctions with func_name
                if hasattr(cls, func_name):
                    
                    # get function from cls
                    func = getattr(cls, func_name)
                    
                    
                    # add data to custom_field_data
                    custom_field_data.update({field_name: func(cls, instance)})
                    
                else:
                    # if cls have not any function with name 'func_name'
                    warning.add_warning("{0} have not any function with name \'{1}\'".format(cls.__name__, func_name))

            
            # get instance data
            data = cls.__get_data(instance, fields=fields, excepted_fields=except_fields)
            
            data.update(custom_field_data)
            data.update(all_r_data)
            # all instance data to all_data
            all_data.append(data)
                

        # show warnings
        warning.show_warnings()
        


        # if many value is not 'None'
        if many != None:
            # cleate empty object
            all_data = [i for i in all_data if i]
            
            # return all data
            return {"data": all_data}
        
        
        # many is None return last item from all data
        return {"data" : all_data[-1]}


    @classmethod
    def __get_attrs(cls, object, name):
        """get arrts from objects

        Args:
            object (class)
            name (str)

        Returns:
            [attr, None]
        """
        if hasattr(object, name):
            return getattr(object, name)
        else:
            return None
      
    @classmethod  
    def __get_multi_data(cls, data_list, fields=[], excepted_fields:list=[]):
        """get multi instance data

        Args:
            data_list (list)
            fields (list, optional): Defaults to [].
            excepted_fields (list, optional): Defaults to [].

        Returns:
            list: list of data
        """
        data = []
        for item in data_list:
            data.append(cls.__get_data(item, fields, excepted_fields))

        return data
    
    @classmethod
    def __get_data(cls, instance, fields=[], excepted_fields:list=[]):
        """get instance data

        Args:
            instance (instance of Model)
            fields (list, optional): Defaults to [].
            excepted_fields (list, optional): Defaults to [].

        Returns:
            dict: instance data
        """
        
        if not isinstance(excepted_fields, (list,tuple)):
            if excepted_fields != None:
                warning.add_warning("excepted_fields only can a list or tuple")
            excepted_fields = list()
            
        excepted_fields.append('_state')
        data = dict()
        if fields != None:
            for key, value in instance.__dict__.items():
                if not key in excepted_fields:
                    if fields == '__all__' or len(fields) == 0 :
                        data.update({key:value})
                    else:
                        if key in fields:
                            data.update({key:value}) 
                    
        
        return data


    class CustomField:
        def __init__(self, func_name, field_name=None):
            self.field_name = field_name
            self.func_name = func_name

    class RelatedField:
        def __init__(self,  relation_type, fields='__all__', except_fields=[], field_name=None):
            self.relation_type = relation_type
            self.fields = fields
            self.except_fields = except_fields
            self.field_name = field_name

