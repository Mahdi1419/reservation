from django.urls import path, include


app_name = 'api_app'

urlpatterns = [
    path('account/', include('account.api.urls')),
    path('reserve/', include('reserve.api.urls')),
]
