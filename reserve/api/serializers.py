
from django.db.models import fields
from api_app.Serializer import RelationTypes, Serializer
from ..models import Doctor, Field, Section, Shift

class SectionSerializer(Serializer):
    
    field = Serializer.RelatedField(RelationTypes.ManyToOne, field_name='fields')
    class Meta:
        model = Section
        fields = ['title', 'id']
        
    
        
        
class FieldSerializer(Serializer):
    section = Serializer.CustomField('get_section')
    
    class Meta:
        model = Field
        fields = ['title']
        
    
    def get_section(self, obj):
        return {
            "id": obj.section.id,
            "title": obj.section.title
        }
        
        
class DoctorSerializer(Serializer):
    
    fields = Serializer.RelatedField(relation_type=RelationTypes.ManyToMany)
    shift = Serializer.RelatedField(relation_type=RelationTypes.ManyToOne, field_name='shifts', except_fields=['doctor_id'])
    fields_section = Serializer.CustomField('get_section_data', field_name='section')
    
    class Meta:
        model = Doctor
        fields = '__all__'
        

class ShiftSerializer(Serializer):
    doctorFullName = Serializer.CustomField("get_doctor_name", field_name='doctor')
    class Meta:
        model = Shift
        fields = "__all__"
        except_fields = ['doctor_id']
        
    
    def get_doctor_name(self, obj):
        name = obj.doctor.name
        family = obj.doctor.family
        return f"{name} {family}"