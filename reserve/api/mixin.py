
from api_app.response import ApiResponse

class AdminRequired:
    def dispatch(self, request, *args, **kwargs):
        if not request.auth_user.is_admin:
            return ApiResponse({"message": "Only admins acssec to this url"}, status=403)

        return super().dispatch(request, *args, **kwargs)