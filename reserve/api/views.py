
from django.db.models import fields
from django.views import View
from Reservation import settings
from account.api.mixin import AuthRequired
from api_app.decorators import parmsCheck
from api_app.response import ApiResponse
from api_app.utils import DateValidate, MessageHandler, TimeValidate, checkListEquality, cleanDicts
from reserve.api.serializers import DoctorSerializer, FieldSerializer, SectionSerializer, ShiftSerializer
from reserve.models import Doctor, Section, Field, Shift
from account.api.decorators import authRequired, adminRequired
from api_app.response import StaticResponse
from django.db.utils import IntegrityError
import json


class SectionsView(View):
    """Get Section model data"""

    def get(self, request):
        """
        Get and return all Sections 
        """

        section_objects = Section.objects.all()
        serialize_data = SectionSerializer(instance=section_objects, many=True)
        return ApiResponse(serialize_data, status=200)

    @authRequired
    @adminRequired
    @parmsCheck(needed_params=['title'], method='POST')
    def post(self, request):
        """Add Section to Sections"""

        try:
            # get request.POST parameter
            params = request.POST

            # create Section instanve
            new_section = Section(
                title=params.get('title')
            )
            new_section.save()

            # return new_section data
            serialize_data = SectionSerializer(instance=new_section)
            return ApiResponse({**serialize_data, "message": "Section create successfully"}, status=201)

        except IntegrityError as e:
            # if section already exist
            if 'duplicate key' in str(e):
                return StaticResponse.AlreadyExist

            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    def put(self, request):
        pass

    def delete(self, request):
        pass


class SectionView(View):
    pass

class FieldsView(View):
    """Get Field model data"""

    def get(self, request):
        """
        Get and return all Fields 
        """

        field_objects = Field.objects.all()
        serialize_data = FieldSerializer(instance=field_objects, many=True)
        return ApiResponse(serialize_data, status=200)

    @authRequired
    @adminRequired
    @parmsCheck(needed_params=['title', 'section'], method='POST')
    def post(self, request):
        try:

            params = request.POST
            # get section
            section_obj = Section.is_exist({
                "id": params.get('section')
            })

            # if section not found
            if not section_obj:
                return StaticResponse.SectionNotFount

            # create new field
            new_field = Field(
                title=params.get('title'),
                section=section_obj
            )
            new_field.save()

            # return new_field data
            serialize_data = FieldSerializer(instance=new_field)
            return ApiResponse({**serialize_data, "message": "field create successfully"}, status=201)

        except IntegrityError as e:
            # if section already exist
            if 'duplicate key' in str(e):
                return StaticResponse.AlreadyExist

            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    def put(self, request):
        pass

    def delete(self, request):
        pass


class FieldView(View):
    pass

class DoctorsView(View):
    """Get Doctor model data"""

    def get(self, request):
        """
        Get and return all Doctors 
        """

        doctor_objects = Doctor.objects.all()
        serialize_data = DoctorSerializer(instance=doctor_objects, many=True)
        return ApiResponse(serialize_data, status=200)

    @authRequired
    @adminRequired
    @parmsCheck(needed_params=['name', 'family', 'fields'], method='POST')
    def post(self, request):
        try:

            params = request.POST

            messageHandler = MessageHandler()

            # create new Doctor
            new_doctor = Doctor(
                name=params.get('name'),
                family=params.get('family')
            )

            new_doctor.save()

            # add fields to doctor
            fields_id = params.getlist('fields')
            for field_id in fields_id:
                try:
                    field = Field.is_exist({
                        "id": int(field_id)
                    })
                    if field:
                        new_doctor.fields.add(field)
                    else:
                        raise Exception()
                except Exception as e:
                    messageHandler.add_message(
                        f'no any field with id ({field_id})')

            new_doctor.save()

            # get all messaages from message handler
            all_messages = messageHandler.get_all_messages()

            # get doctor date
            serialize_data = DoctorSerializer(instance=new_doctor)
            return ApiResponse({**serialize_data, "message": "doctor registration was successfully", **all_messages}, status=201)

        except IntegrityError as e:
            # if section already exist
            if 'duplicate key' in str(e):
                return StaticResponse.AlreadyExist

            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    def put(self, request):
        pass

    def delete(self, request):
        pass


class DoctorView(View):
    # single doctor view

    def get(self, request, slug):
        """get and return a doctor"""
        try:

            # get doctor
            doctor_object = Doctor.is_exist({
                "slug": slug
            })

            # if not exist any doctor with this slug
            if not doctor_object:
                return ApiResponse({
                    "message": "Doctro not found"
                }, status=404)

            # get doctor date
            serialize_data = DoctorSerializer(instance=doctor_object)
            return ApiResponse({**serialize_data, "message": "Doctor founded"}, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    @authRequired
    @adminRequired
    def post(self, request, slug):
        """update a doctor"""

        try:

            params = request.POST

            # get doctor
            doctor_object = Doctor.is_exist({
                "slug": slug
            })


            # if no doctor with this slug exists
            if not doctor_object:
                return ApiResponse({
                    "message": "Doctro not found"
                }, status=404)

            
            # update doctor
            update = doctor_object.custom_update(params, except_update=['id', 'slug'])

            if not update:
                return ApiResponse({
                    "message": "update failed" 
                }, status=400)
                
                
            
            # get doctor date
            serialize_data = DoctorSerializer(instance=update)
            return ApiResponse({**serialize_data, "message": "Doctor founded"}, status=202)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


    @authRequired
    @adminRequired
    def delete(self, request, slug):

        try:        
            # get doctor
            doctor_object = Doctor.is_exist({
                "slug": slug
            })
            
            # if no doctor with this slug exists
            if not doctor_object:
                return ApiResponse({
                    "message": "Doctro not found"
                }, status=404)


            # delete doctor
            doctor_object.delete()
            
            return ApiResponse({
                "message": "Doctor successfully deleted"
            })
            
        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


class ShiftsView(AuthRequired, View):
    """Get Shift model data"""

    def get(self, request):
        try:
            """
            Get and return all shifts 
            """
            shift_object = Shift.objects.all()
            
            
            serialize_data = ShiftSerializer(instance=shift_object, many=True if id == None else False)
            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    @authRequired
    @adminRequired
    @parmsCheck(needed_params=['day', 'times', 'doctor'], method='POST')
    def post(self, request, id=None):

        try:
            # get request data
            params = request.POST

            messageHandler = MessageHandler()

            # get doctor
            doctor_obj = Doctor.is_exist({
                "id": params.get('doctor')
            })

            if not doctor_obj:
                return ApiResponse({
                    "message": "doctor with with id '{0}' not found".format(params.get('doctor'))
                })

            # check date froman
            date_validation = DateValidate(params.get('day'))
            if not date_validation:
                return ApiResponse({
                    'message': "day format invalid (valid: 2021-01-01)"
                }, 400)

            print(date_validation)
            # create Shift instance
            new_shift = Shift(
                day=date_validation,
                doctor=doctor_obj
            )

            # extract shift times from request data
            times = params.getlist('times')
            times_list = list()
            for t in times:
                try:
                    time_validation = TimeValidate(t)
                    if time_validation:
                        times_list.append(t)
                    else:
                        raise Exception()
                except Exception as e:
                    messageHandler.add_message(
                        "{0} format invalid (valid: 00:00:00)".format(t))

            # add times list to new shift instance
            new_shift.times = times_list

            # if no problem save new shift instance
            new_shift.save()

            # get all messaages from message handler
            all_messages = messageHandler.get_all_messages()

            # get new shift data
            serialize_data = ShiftSerializer(instance=new_shift)
            response = {**serialize_data, "message": "shift set successfully"}
            if all_messages:
                response.update(all_messages)

            return ApiResponse(response, status=201)

        except IntegrityError as e:
            # if section already exist
            if 'duplicate key' in str(e):
                return StaticResponse.AlreadyExist

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


class ShiftView(View):
    def get(self, request, id):
        """get and return a shift"""
        try:
            
            # get shift
            shift_object = Shift.is_exist({
                "id": id
            })
            
            # if no shift
            if not shift_object:
                return ApiResponse({
                    "message": 'no shift with id {0} exists'.format(id)
                }, status=404)
            
            
            # get shif date
            serialize_data = ShiftSerializer(instance=shift_object)
            return ApiResponse({**serialize_data, "message": "Shift founded"}, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


    @authRequired
    @adminRequired
    @parmsCheck(needed_params=['times'], method='POST')
    def post(self, request, id):
        
        try:
            messageHandler = MessageHandler()
            
            params = request.POST
        

        
            # get shift
            shift_object = Shift.is_exist({
                "id": id
            })
            
            # if no shift
            if not shift_object:
                return ApiResponse({
                    "message": 'no shift with id {0} exists'.format(id)
                }, status=404)
            
            
            times = params.getlist('times')
            times_list = list()
            for t in times:
                try:
                    time_validation = TimeValidate(t)
                    if time_validation:
                        times_list.append(t)
                    else:
                        raise Exception()
                except Exception as e:
                    messageHandler.add_message(
                        "{0} format invalid (valid: 00:00:00)".format(t))
                        
            
            # update shift
            new_data = {
                "times" : times_list
            }
            
            update = shift_object.custom_update(new_data, except_update=['id', 'doctor', 'day_in_week', 'day'])
            if not update:
                return ApiResponse({
                    "message": "update failed",
                }, status=400)

            # get all messaages from message handler
            all_messages = messageHandler.get_all_messages()

            # get new shift data
            serialize_data = ShiftSerializer(instance=update)
            response = {**serialize_data, "message": "shift set successfully"}
            if all_messages:
                response.update(all_messages)

            return ApiResponse(response, status=201)
            
        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError
                

class ReserveView(AuthRequired, View):
    pass
