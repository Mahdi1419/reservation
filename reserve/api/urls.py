from django.urls import path
from .views import (
    DoctorsView, DoctorView,
    FieldsView, FieldView,
    ReserveView,
    SectionsView, SectionView,
    ShiftsView, ShiftView,
)




urlpatterns = [
    path('doctor/', DoctorsView.as_view(), name='doctors'),
    path('doctor/<slug:slug>', DoctorView.as_view() , name='doctor'),
    path('shift/', ShiftsView.as_view(), name='shifts'),
    path('shift/<int:id>', ShiftView.as_view(), name='shift'),
    path('field/', FieldsView.as_view(), name='fields'),
    path('field/<int:id>', FieldView.as_view(), name='field'),
    path('section/', SectionsView.as_view(), name='sections'),
    path('section/<int:id>', SectionView.as_view(), name='section'),
    path('add/', ReserveView.as_view(), name='reserves'),
]
