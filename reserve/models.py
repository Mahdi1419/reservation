from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from account.models import Account
from api_app.utils import SuperModel, create_receipt_token, get_random_string
from django_better_admin_arrayfield.models.fields import ArrayField
# Create your models here.


class Section(models.Model, SuperModel):
    """
    Model for Different wards of the hospital like (child's ,  Women, Neurology, ...)\n
    Properties: 
    * title => title of section
    """
    title = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.title


class Field(models.Model, SuperModel):
    """
    Model for title and specialization of physicians\n
    Properties: 
    * title => title of work field
    * section => instance of 'Section' model
    """
    title = models.CharField(max_length=255)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('title', 'section',)

    def __str__(self):
        return self.title


class Doctor(models.Model, SuperModel):
    """
    Doctors model\n
    Properties:
    * name
    * family
    * rate
    * fields => list of Field instances 
    """
    # account = models.ForeignKey(to=Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=False, null=False)
    family = models.CharField(max_length=100, blank=False, null=False)
    fields = models.ManyToManyField(to=Field)
    slug = models.SlugField(blank=True, null=True)
    rate = models.IntegerField(
        validators=[MaxValueValidator(10), MinValueValidator(0)],
        default=0
    )

    class Meta:
            # Two instance can not be same with this fields
        unique_together = ('name', 'family')

    def __str__(self):
        return f'{self.name.capitalize()} {self.family.capitalize()}'


class Shift(models.Model, SuperModel):
    """Doctors shifs model

    Properties:
    * day (Date) => year-month-day
    * times (Time) => hour:min:sec
    * doctor (Doctor)
    """
    day = models.DateField()
    times = ArrayField(models.TimeField())
    day_in_week = models.CharField(max_length=3, blank=True, null=True)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)

    class Meta:
        # Two instance can not be same with this fields
        unique_together = ('doctor', 'day',)

    def save(self, *args, **kwargs):
        
        # Remove 'times' duplicates
        self.times = list(dict.fromkeys(self.times))
        
        # Before save get name of day in week and store it
        self.day_in_week = self.day.strftime("%a")
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.doctor} : {self.day_in_week} {self.day}'


class Reserve(models.Model, SuperModel):
    """
    Model for recording the turns taken\n
    Properties:
    * user => instance of "User" model
    * doctor => instance of "Doctor"
    * field => instance of "Field"
    * reserve_day => DateField
    * reserve_at => List of TimeField
    """
    

    
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE, blank=True, null=True)
    reserve_day = models.ForeignKey(to=Shift, on_delete=models.CASCADE)
    reserve_at = models.TimeField()
    receipt_token = models.CharField(
        max_length=255, default = create_receipt_token)


    class Meta:
        unique_together = ('doctor', 'reserve_day','reserve_at')

    def save(self, *args, **kwargs):

        # checking reserve time in doctor shifts    
        if not self.reserve_at in self.reserve_day.times:
            raise Exception('The selected time is not a selectable time')

        if not self.field in self.doctor.field_set.all():
            raise Exception('The selected field is not one of the doctor\'s fields')
        
        if self.doctor.account == self.account:
            raise Exception('Doctot and Account can\'t be same')

        # set section from reserve field
        self.section = self.field.section
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.account} : {self.receipt_token}'
