# Generated by Django 3.2.9 on 2021-11-10 15:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
        ('reserve', '0009_reserve_section'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='accoount', to='account.account'),
        ),
        migrations.AlterField(
            model_name='reserve',
            name='section',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='reserve.section'),
        ),
        migrations.AlterUniqueTogether(
            name='reserve',
            unique_together={('doctor', 'reserve_day', 'reserve_at')},
        ),
    ]
