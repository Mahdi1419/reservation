


from django.db.models.signals import post_save, pre_delete, post_delete, pre_save
from django.dispatch import receiver
from django.utils.text import slugify
from .models import Doctor

@receiver(pre_save, sender=Doctor)
def post_slug_create(sender, instance, **kwargs):
    slug = f'{instance.name} {instance.family}'
    instance.slug = slugify(slug)
        
