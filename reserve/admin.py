from django.contrib import admin
from django.db import models
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin

from .models import Doctor, Field, Section, Reserve, Shift
# Register your models here.

class ReserveDayAdmin(admin.ModelAdmin, DynamicArrayMixin):
    pass


class DoctorAdmin(admin.ModelAdmin):
    list_display  = ('name','family' ,'slug',)

admin.site.register((Field, Section, Reserve))
admin.site.register(Doctor, DoctorAdmin)
admin.site.register(Shift, ReserveDayAdmin)